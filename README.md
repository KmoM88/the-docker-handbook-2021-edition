# The Docker Handbook – 2021 Edition

https://www.freecodecamp.org/news/the-docker-handbook/
<h2>Docker</h2>
<h3>Hello World</h3>
<ul>
    <li>docker run hello-world</li>
    <li>docker ps</li>
    <li>docker ps -a</li>
</ul>

<p>Usan en mismo kernel</p>
<ul>
    <li>uname -a</li>
    <li>docker run alpine uname -a</li>
</ul>

<h3>Docker Container Manipulation Basics</h3>
<ul>
    <li>docker run "image name"</li>
    <li>docker "object" "command" "options"</li>
</ul>
<h3>How to Publish a Port</h3>
<ul>
    <li>docker container run --publish 8080:80 fhsinchy/hello-dock</li>
    <li>docker container run --detach --publish 8080:80 fhsinchy/hello-dock</li>
</ul>
<h3>How to Name or Rename a Container</h3>
<ul>
    <li>docker container run --detach --publish 8888:80 --name hello-dock-container fhsinchy/hello-dock</li>
    <li>docker container rename gifted_sammet hello-dock-container-2</li>
</ul>
<h3>How to Stop or Kill a Running Container</h3>
<ul>
    <li>docker container stop hello-dock-container</li>
    <li>docker container kill hello-dock-container-2</li>
</ul>
<h3>How to Restart a Container</h3>
<ul>
    <li>docker container start hello-dock-container</li>
    <li>docker container restart hello-dock-container-2</li>
</ul>
<h3>How to Create a Container Without Running</h3>
<ul>
    <li>docker container create --publish 8080:80 --name hello-dock-container-3 fhsinchy/hello-dock</li>
    <li>docker container ls --all</li>
    <li>docker container start hello-dock-container-3 </li>
</ul>
<h3>How to Remove Dangling Containers</h3>
<ul>
    <li>sudo docker ps -a | grep Exit | cut -d ' ' -f 1 | xargs sudo docker rm</li>
</ul>
<h4>There is also the --rm option for the container run  and container start commands which indicates that you want the containers removed as soon as they're stopped. </h4>
<ul>
    <li>docker container run --rm --detach --publish 8888:80 --name hello-dock-volatile fhsinchy/hello-dock</li>
</ul>
<h3>How to Run a Container in Interactive Mode</h3>
<ul>
    <li>docker container run --rm -it ubuntu</li>
    <li>docker container run -it node</li>
</ul>
<h3>How to Execute Commands Inside a Container</h3>
<ul>
    <li>docker container run --rm busybox echo -n my-secret | base64</li>
</ul>
<h3>How to Work With Executable Images</h3>
<ul>
    <li>mkdir files && cd files</li>
    <li>touch a.pdf b.pdf c.txt d.pdf e.txt && cd ..</li>
    <li>docker container run --rm -v files:/zone fhsinchy/rmbyext pdf</li>
</ul>
<h3>How to Create a Docker Image</h3>
<ul>
    <li>docker container run --rm --detach --name default-nginx --publish 8080:80 nginx</li>
    <li>Dockerfile</li>
    <li>docker image build --tag custom-nginx:packaged ./Nginx-from-ubuntu</li>
</ul>
<h3>How to List and Remove Docker Images</h3>
<ul>
    <li>docker image ls</li>
    <li>docker image rm tag</li>
</ul>
<h3>How to Understand the Many Layers of a Docker Image</h3>
<ul>
    <li>docker image history custom-nginx:packaged</li>
</ul>
<h3>How to Build NGINX from Source</h3>
<ul>
    <li>docker image build --tag kmom/custom-nginx:built ./Nginx-from-source-ubuntu</li>
    <li>docker image build --tag kmom/custom-nginx:built ./Nginx-from-source-ubuntu-arguments</li>
    <li>docker container run --rm --detach --name custom-nginx-built --publish 8080:80 custom-nginx:built</li>
</ul>
<h3>How to Optimize Docker Images</h3>
<ul>
    <li>docker image pull nginx:stable</li>
    <li>docker image build --tag kmom/custom-nginx:built ./Nginx-from-source-ubuntu-optimize</li>
</ul>
<h3>Embracing Alpine Linux</h3>
<ul>
    <li>docker image build --tag kmom/custom-nginx:built ./Nginx-from-alpine</li>
</ul>
<h3>How to Create Executable Docker Images</h3>
<ul>
    <li>docker image build --tag rmbyext ./Executable-python</li>
</ul>
<h3>How to Share Your Docker Images Online</h3>
<ul>
    <li>docker login</li>
    <li>docker image push kmom/custom-nginx:built</li>
</ul>
<h2>How to Containerize a JavaScript Application</h2>
<h3>How to Write the Development Dockerfile</h3>
<ul>
    <li>docker image build --tag kmom/hello-dock:dev ./Container-node-app</li>
    <li>docker container run --rm --detach --publish 8080:8080 --name hello-dock-dev kmom/hello-dock:dev</li>
</ul>
<h3>How to Perform Multi-Staged Builds in Docker</h3>
<ul>
    <li>docker image build --tag kmom/hello-dock:dev ./Multi-stage</li>
    <li>docker container run --rm --detach --publish 8080:80 --name hello-dock-dev kmom/hello-dock:dev</li>
</ul>
<h3>Network Manipulation Basics in Docker</h3>
<ul>
    <li>docker network ls</li>
    <li>docker network create skynet</li>
    <li>docker network connect skynet hello-dock</li>
    <li>docker container run --network skynet --rm --name alpine-box -it alpine sh</li>
    <li>docker network disconnect skynet hello-dock</li>
    <li>docker network rm skynet</li>
</ul>
<h2>Docker-Compose</h2>
